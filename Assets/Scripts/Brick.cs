﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {

    public AudioClip crack;
    public int maxHits;
    private int timesHit;
    private LevelManager levelManager;
    public Sprite[] hitSprites;

    public GameObject smoke;

	// Use this for initialization
	void Start () {
        timesHit = 0;
        levelManager = GameObject.FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {

        if (timesHit >= maxHits)
        {
            levelManager.BrickDestroyed();
            //GameObject newSmoke = Instantiate(smoke, transform.position, Quaternion.identity);
            //newSmoke.GetComponent<ParticleSystem>().startColor = this.GetComponent<SpriteRenderer>().color;
            GameObject.Destroy(gameObject);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
      //  AudioSource.PlayClipAtPoint(crack, transform.position);
        timesHit++;
        if (timesHit < maxHits)
            LoadSprites();
    }

    void LoadSprites()
    {
        int spriteIndex = timesHit - 1;
        this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
    }

    void SimulateWin()
    {
        levelManager.LoadNextLevel();
    }
}
