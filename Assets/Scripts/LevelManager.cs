﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    private GameObject[] allBricks;
    private int brickCount;

    private void Start()
    {
        allBricks = GameObject.FindGameObjectsWithTag("Brick");
        brickCount = allBricks.Length;
    }

    public void LoadLevel(string name)
    {
        Application.LoadLevel(name);
    }

    public void QuitRequest()
    {
        Debug.Log("Quit request registered");
        Application.Quit();
    }

    public void LoadNextLevel()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
    }

    public void BrickDestroyed()
    {
        brickCount--;
        Debug.Log(brickCount);
        if(brickCount <= 0)
            LoadNextLevel();
    }
}
