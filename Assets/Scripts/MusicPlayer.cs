﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    //creating a singleton instance
    public static MusicPlayer instance;

    void Awake()
    {
        if (instance == null) //initialize first instance
            instance = this;
        else //destroy other instances
            GameObject.Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
        //keeps the track playing across all levels
        GameObject.DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
