﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    public bool autoPlay = false;
    private Ball ball;
	// Use this for initialization
	void Start () {
        ball = GameObject.FindObjectOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!autoPlay)
            MoveWithMouse();
        else
            AutoPlay();
    }

    void AutoPlay()
    {
        Vector3 paddlePos = new Vector3(0.5f, this.transform.position.y, 0f);
        //gets mouse position in blocks 0 to 16
        Vector3 ballPos = ball.transform.position;
        //moves the paddle with the mouse between 0 and 16
        this.transform.position = new Vector3(Mathf.Clamp(ballPos.x, 0.5f, 15.5f), this.transform.position.y, this.transform.position.z);
    }

    void MoveWithMouse()
    {
        //gets mouse position in blocks 0 to 16
        float mousePosInBlocks = (Input.mousePosition.x / Screen.width * 16);
        //moves the paddle with the mouse between 0 and 16
        this.transform.position = new Vector3(Mathf.Clamp(mousePosInBlocks, 0.5f, 15.5f), this.transform.position.y, this.transform.position.z);
    }
}
